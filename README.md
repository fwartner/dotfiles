dotfiles
========

My personal dotfiles used on my MacBook Air

Usage
-----

Simply clone this repository to your `~/`:

`git clone https://github.com/fwartner/dotfiles.git .`

To load each file, add the following lines to your profile:

```
# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{path,bash_prompt,exports,aliases,functions,extra}; do
  [ -r "$file" ] && source "$file"
done
unset file
```
